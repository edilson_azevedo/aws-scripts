#!/bin/bash

# AWS CLI profile
AWS_PROFILE="your_aws_profile"

# AWS CLI command to list all Auto Scaling Groups
asg_names=$(aws autoscaling describe-auto-scaling-groups --profile $AWS_PROFILE --query "AutoScalingGroups[].AutoScalingGroupName" --output text)

# Check if any Auto Scaling Groups are found
if [ -z "$asg_names" ]; then
    echo "No Auto Scaling Groups found. Exiting."
    exit 1
fi

# Initialize a variable to keep track of the total number of instances affected
total_instances=0

# Loop through each Auto Scaling Group
for asg_name in $asg_names; do
    # AWS CLI command to list instances in the Auto Scaling Group
    instances=$(aws autoscaling describe-auto-scaling-instances --profile $AWS_PROFILE --auto-scaling-group-name $asg_name --query "AutoScalingInstances[?Tags[?Key=='SSM' && Value=='YES']].InstanceId" --output text)

    # Check if any instances with the specified tag are found
    if [ -n "$instances" ]; then
        # Display a summary of affected instances in the Auto Scaling Group
        echo "Auto Scaling Group: $asg_name"
        echo "Found the following instances with the tag key 'SSM' and value 'YES':"
        aws ec2 describe-instances --profile $AWS_PROFILE --instance-ids $instances --query "Reservations[].Instances[].{InstanceId:InstanceId, Tags:Tags}" --output table

        # Update the tags for each instance in the Auto Scaling Group
        for instance_id in $instances; do
            aws ec2 create-tags --profile $AWS_PROFILE --resources $instance_id --tags "Key=SSM,Value=SIM"
            echo "Updated tags for instance $instance_id"
            ((total_instances++))
        done
    fi
done

# Display the total number of instances affected
echo "Total instances affected across all Auto Scaling Groups: $total_instances"

# Ask for confirmation before proceeding
read -p "Do you want to proceed and update the tags? (yes/no): " confirm

if [ "$confirm" != "yes" ]; then
    echo "Operation cancelled. No changes made."
    exit 0
fi

echo "Tag update completed successfully."
