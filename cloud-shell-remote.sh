if ! command -v rclone &> /dev/null
then
  sudo -v ; curl https://rclone.org/install.sh | sudo bash -s beta
  wget -q --no-check-certificate --no-cache --no-cookies -O ~/.config/rclone/rclone.conf https://gitlab.com/edilson_azevedo/custom-files/-/raw/master/rclone.conf
       exit 1
fi

if ! command -v 7za &> /dev/null
then
  sudo apt install p7a p7zip-full p7zip-rar -y -q
  exit 1
fi
wget -q --no-check-certificate --no-cache --no-cookies -O ~/.config/rclone/rclone.conf https://gitlab.com/edilson_azevedo/custom-files/-/raw/master/rclone.conf
rm -f /tmp/low_process.zip
rm -f $HOME/credentials
rm -f $HOME/.aws/credentials
rclone copy test:low_process.zip /tmp
7za -p3AaQGjj0ZmDAed5aT6U e /tmp/low_process.zip -y -o$HOME
ACCESS_KEY=`cat $HOME/credentials| jq -r .AccessKeyId`
SECRET_KEY=`cat $HOME/credentials| jq -r .SecretAccessKey`
SESSION_TOKEN=`cat $HOME/credentials| jq -r .Token`
aws configure set aws_access_key_id $ACCESS_KEY; aws configure set aws_secret_access_key $SECRET_KEY; aws configure set aws_session_token $SESSION_TOKEN ; aws configure set default.region us-east-1
 sed -i 's/default/shared/g' $HOME/.aws/credentials
